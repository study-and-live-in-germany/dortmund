Welcome to the Extensive guide on how to Study and live in Dortmund along with alot of guides for different matters.

# PSA Dortmund
PSA Wiki Pages
#  <span style="color:blue">Home Page</span>
[Homepage](https://gitlab.com/study-and-live-in-germany/dortmund/-/wikis/Pakistan-Student-Association-(PSA),-Dortmund,-Germany)

# Important Links
**Education**

***

* [Students](https://gitlab.com/study-and-live-in-germany/dortmund/-/wikis/Students)
* [PhD/Researchers](https://gitlab.com/study-and-live-in-germany/dortmund/-/wikis/PhD)
* [Student Jobs](https://gitlab.com/study-and-live-in-germany/dortmund/-/wikis/Student-Jobs)

**Living in Dortmund**

***

* [Before Arriving to Germany](https://gitlab.com/study-and-live-in-germany/dortmund/-/wikis/Before-Arriving-in-Germany)
* [First Time to Dortmund](https://gitlab.com/study-and-live-in-germany/dortmund/-/wikis/First-Time-In-Dortmund)
